class Config(object):
    DEBUG = False
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class ProductionConfig(Config):
    ## uses a docker instance
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:PASSWORD@10.42.0.57:33060/authors'

class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///authors.sqlite"
    SQLALCHEMY_ECHO = False

class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:PASSWORD@10.0.0.20:33060/authors'
    SQLALCHEMY_ECHO = False