from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, From Flask!'


## to run ENTER
## FLASK_APP=app.py flask run